﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zad1
{
    public partial class Form1 : Form
    {
        double x, y, rez;
        public Form1()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox3.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 3!", "Pogreška");
            }
            else
            {
                rez = Math.Cos(x);
                textBox4.Text = rez.ToString();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            else if (!double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            else
            {
                rez = x + y;
                textBox4.Text = rez.ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            else if (!double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            else
            {
                rez = x - y;
                textBox4.Text = rez.ToString();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            else if (!double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            else
            {
                rez = x * y;
                textBox4.Text = rez.ToString();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox3.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 3!", "Pogreška");
            }
            else
            {
                rez = Math.Sin(x);
                textBox4.Text = rez.ToString();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox3.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 3!", "Pogreška");
            }
            else
            {
                rez = Math.Sqrt(x);
                textBox4.Text = rez.ToString();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox3.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 3!", "Pogreška");
            }
            else
            {
                rez = Math.Log(x);
                textBox4.Text = rez.ToString();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox3.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 3!", "Pogreška");
            }
            else
            {
                rez = x * x;
                textBox4.Text = rez.ToString();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rez = x / y;
            textBox4.Text = rez.ToString();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
